- Flex boxes are good for situations when you need a very simple layout on a webpage. [Grid Layout
and Content Properties]
- When a layout gets more complicated, such as multiple rows and columns, you will use a . [Grid
Layout and Content Properties]
- Another type of grid is a grid template although it is not very widely supported. [Grid Templates]
- A grid template is a string of characters that creates a row and each character in that string is a column.
[Grid Templates]