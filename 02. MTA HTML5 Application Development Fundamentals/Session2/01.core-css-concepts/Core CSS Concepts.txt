- CSS is not a new set of markup tags, rather it is code that enhances markup tags from a formatting standpoint. [Purpose of CSS]
- CSS is presentation based. [Purpose of CSS]
- There are three types of styles that you can put on webpages: an Inline style, an internal style sheet,
and an external style sheet. [Purpose of CSS]
- An inline style is a style that you apply directly to an HTML tag. [Inline Styles]
- An internal style sheet is simply a style that is internal to a single page. [Internal Style Sheets]
- Any internal style sheet you create will be placed inside the head tag. [Internal Style Sheets]
- Not all styles you put in a style sheet will be tag specific. [Classes]
- A class is a style that you can apply anywhere, anytime. [Classes]
- When choosing colors for text avoid  red,blue , and purple because they can
be confused as hyperlinks. [Classes]
- When working with hexadecimal colors, remember that closer to all zeros is darker, and closer to all F�s is
lighter. [IDs and Imports]
- Internal style sheets will win out over external style sheets. [Inheritance and Overrides]
- Content flow comes in two forms, either inline flow or block flow. [Content Flow]
- The real reason for using float is to control how text displays on devices with different widths of
resolution. [Floating Text]